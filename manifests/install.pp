# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include pseint::install
class pseint::install {
  file { $pseint::package_extract_path:
    ensure  => directory,
  } ->
  archive { "/tmp/${pseint::package_name}.tgz":
    ensure        => $pseint::package_ensure,
    extract       => $pseint::package_extract,
    extract_path  => $pseint::package_extract_path,
    source        => "${pseint::package_repo}/${pseint::package_name}-${pseint::package_arch}-${pseint::package_release}.tgz",
    creates       => "${pseint::package_extract_path}/${pseint::package_name}",
    cleanup       => $pseint::package_cleanup,
  }

  file { $pseint::launcher_path:
    ensure  => directory,
  } ->
  file { "${pseint::launcher_path}/${pseint::launcher}":
    ensure => $pseint::launcher_ensure,
    source => $pseint::launcher_source,
    mode   => '0644',
  }
}
