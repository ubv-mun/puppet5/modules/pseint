# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include pseint
class pseint (
  String  $package_name,
  String  $package_ensure,
  String  $package_arch,
  String  $package_release,
  Boolean $package_extract,
  String  $package_extract_path,
  String  $package_repo,
  Boolean $package_cleanup,
  String  $launcher,
  String  $launcher_path,
  String  $launcher_ensure,
  String  $launcher_source,
) {
  contain 'pseint::install'
}
